# Marty's bash run-com

# stop now if not interactive bash
[ -z "$BASH" -o -z "$PS1" ] && return 0

PS1='\[\e[32m\]\h:\W[\j]\$\[\e[0m\] '
TIMEFORMAT="------> %U user; %S sys; %R real; %P%% CPU"

HISTTIMEFORMAT="%Y-%m-%d %H:%M:%S  "
HISTCONTROL=ignoreboth
HISTSIZE=1000
unset HISTFILE

IGNOREEOF=7
set -o noclobber
set -o notify
shopt -s checkwinsize

# alias settings
alias ls='ls -F'
alias ll='ls -lh'
alias la='ls -A'

function xterm-title() { echo -ne "\e]0;$*\e\\"; }
function xterm-cursor() { echo -ne "\e]12;$1\e\\"; }

# [ -f /etc/bash_completion ] && . /etc/bash_completion

