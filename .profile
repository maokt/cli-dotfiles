#!/bin/bash
# this may not work on non-bash login shells

umask 077
ulimit -S -c 0

PATH=/usr/sbin:/usr/bin:/sbin:/bin
[ -d ~/bin ] && PATH=~/bin:"$PATH"

export LANG=en_GB.UTF-8
export EDITOR=vim
export VISUAL=$EDITOR
export PAGER=less
export LESS=MRSiqs
export LESSCHARSET=utf-8
[ -x /usr/bin/lessfile ] && eval $(lessfile)
export PKG_CONFIG_PATH=$HOME/lib/pkgconfig
export PERL5LIB=~/perl5/lib/perl5
export GOPATH=$HOME/go

SHORTHOSTNAME=${HOSTNAME%%.*}
[ -f ~/.profile.$SHORTHOSTNAME ] && . ~/.profile.$SHORTHOSTNAME

if [ "$BASH" -a "$PS1" ]
then
    [ -r ~/.bashrc ] && . ~/.bashrc
    [ "$TERM" = "linux" ] && echo -e '\e%GUTF8 mode selected'
fi

